// Saga
import { all, call, put, takeLatest } from 'redux-saga/effects';

// Action types
import { userActionTypes } from '../../actions/user';
// Apis
import { loginApi } from '../../apis/user';

function* userLogin(action) {
    const params = action;
    try {
        const payload = yield call(loginApi, params.payload);
        yield put({ type: userActionTypes.USER_LOGIN_SUCCESS, payload });
        yield put({ type: userActionTypes.GET_USER_INFO });
    } catch (error) {
        yield put({ type: userActionTypes.USER_LOGIN_FAILURE, error });
    }
}

export default function* authSaga() {
    yield all([takeLatest(userActionTypes.USER_LOGIN, userLogin)]);
}
