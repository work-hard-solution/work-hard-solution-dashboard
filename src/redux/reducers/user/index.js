/* eslint-disable no-case-declarations */
import { userActionTypes } from '../../actions/user';

const defaultState = {
    fetching: {},
    error: null,
    loading: false,
    userInfo: null,
};

const userReducer = (state = defaultState, { type, payload, error }) => {
    switch (type) {
        // Common
        // case userActionTypes.COMMON_SUCCESS:
        //     return { ...state, loading: false };
        // case userActionTypes.COMMON_FAILED:
        //     console.log('error common_____', error);
        //     return { ...state, loading: false, error: payload };

        // Login
        case userActionTypes.USER_LOGIN:
            return {
                ...state,
                loading: true,
                userInfo: null,
            };

        case userActionTypes.USER_LOGIN_SUCCESS:
            return {
                ...state,
                loading: false,
                userInfo: payload,
            };

        case userActionTypes.USER_LOGIN_FAILURE:
            return {
                ...state,
                loading: false,
                error,
            };

        case userActionTypes.USER_FORGOT_PASSWORD:
            return {
                ...state,
                loading: true,
                userInfo: null,
            };

        case userActionTypes.RESET_STATE_BY_KEY:
            return {
                ...state,
                [payload.key]: payload.value || null,
            };
        // Register
        case userActionTypes.USER_REGISTER:
            return {
                ...state,
                registerFailure: null,
                userInfo: null,
            };
        case userActionTypes.USER_REGISTER_SUCCESS:
            const { tokens, user } = payload;
            localStorage.setItem('access_token', tokens.access_token);
            return {
                ...state,
                userInfo: user,
            };
        default:
            return state;
    }
};

export default userReducer;
