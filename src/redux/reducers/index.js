import history from 'configs/history';
import { connectRouter } from 'connected-react-router';
import { combineReducers } from 'redux';

import userReducer from './user';

const rootReducer = combineReducers({
    user: userReducer,
    router: connectRouter(history),
});

export default rootReducer;
