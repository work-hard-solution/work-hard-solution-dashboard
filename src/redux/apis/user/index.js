/* eslint-disable import/no-unresolved */
import request from 'utils/handleAPI';

export function loginApi(payload) {
    return request.post('/auth/login', payload);
}

export function registerApi(payload) {
    return request.post('/auth/register', payload);
}
