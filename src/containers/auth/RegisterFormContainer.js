import { RegisterForm } from 'Forms/RegisterForm';
// Library
// import { noop } from 'lodash';
// import PropTypes from 'prop-types';
// React
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
// Action
import { userRegister } from 'redux/actions/user';

export const RegisterFormContainer = () => {
    const dispatch = useDispatch();
    const history = useHistory();
    // const messageError = useSelector(selectors.messageError());

    const handleSubmit = async (values) => {
        dispatch(userRegister(values));
        history.push('/');
    };

    return <RegisterForm onSubmit={handleSubmit} />;
};

RegisterFormContainer.propTypes = {};

RegisterFormContainer.defaultProps = {};
