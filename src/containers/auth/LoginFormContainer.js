import { LoginForm } from 'Forms/LoginForm';
// Library
// import { noop } from 'lodash';
// import PropTypes from 'prop-types';
// React
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router';
// Action
import { userLogin } from 'redux/actions/user';

export const LoginFormContainer = () => {
    const dispatch = useDispatch();
    const error = useSelector((state) => state.user.error);

    const history = useHistory();

    const handleSubmit = async (values) => {
        dispatch(userLogin(values));

        history.push('/login');
    };

    // const handleOnChange = async () => {
    //     if (error) dispatch(resetStateByKey({ key: 'error' }));
    // };
    return <LoginForm error={error} onSubmit={handleSubmit} />;
};

LoginFormContainer.propTypes = {};

LoginFormContainer.defaultProps = {};
