/* eslint-disable import/no-unresolved */
import { ForgotPasswordForm } from 'Forms/ForgotPasswordForm';
// Library
// import { noop } from 'lodash';
// import PropTypes from 'prop-types';
// React
import React from 'react';
import { useDispatch } from 'react-redux';
import { useHistory } from 'react-router';
import { userForgotPassword } from 'redux/actions/user';

export const ForgotPasswordContainer = () => {
    const dispatch = useDispatch();

    const history = useHistory();

    const handleSubmit = async (values) => {
        dispatch(userForgotPassword(values));
        history.push('/login');
    };

    return <ForgotPasswordForm onSubmit={handleSubmit} />;
};

ForgotPasswordContainer.propTypes = {};

ForgotPasswordContainer.defaultProps = {};
