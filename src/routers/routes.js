import React from 'react';
// Constant
import { PAGES, PATH } from 'utils/constants';

// Pages
const LoginPage = React.lazy(() => import('pages/Auth/Login'));
const RegisterPage = React.lazy(() => import('pages/Auth/Register'));
const ForgotPasswordPage = React.lazy(() => import('pages/Auth/ForgotPassword'));

export const publicPages = [
    {
        path: PATH.AUTH.LOGIN,
        name: PAGES.NAME.AUTH.LOGIN,
        component: LoginPage,
        auth: true,
        exact: true,
    },
    {
        path: PATH.AUTH.REGISTER,
        name: PAGES.NAME.AUTH.REGISTER,
        component: RegisterPage,
        auth: true,
        exact: true,
    },
    {
        path: PATH.AUTH.FORGOT_PASSWORD,
        name: PAGES.NAME.AUTH.FORGOT_PASSWORD,
        component: ForgotPasswordPage,
        auth: false,
        exact: true,
    },
];

export const privatePages = [];
