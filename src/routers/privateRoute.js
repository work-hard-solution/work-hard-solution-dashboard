// Library
import { isNull } from 'lodash';
import PropTypes from 'prop-types';
// React
import React from 'react';
import { Redirect, Route } from 'react-router-dom';
// Constants
import { PATH, TYPES } from 'utils/constants';
// Selectors
// import * as selectors from 'store/auth/selectors';
// import * as commonSelectors from 'store/common/selectors';
import authUtils from 'utils/storage';

const { getSession } = authUtils();

const PrivateRoute = ({ component: Component, needOrganization, ...route }) => {
    // Get token
    const token = getSession([TYPES.ACCESS_TOKEN]);

    const isAuthorized = !isNull(token);

    console.log('isAuthorized', isAuthorized);

    if (['/login', '/signup'].includes(route.path))
        return (
            <Route
                {...route}
                render={(props) =>
                    isAuthorized ? (
                        <Component {...props} />
                    ) : (
                        <Redirect to={{ pathname: `/choose-your-organization` }} />
                    )
                }
            />
        );

    return (
        <Route
            {...route}
            render={(props) => {
                if (!isAuthorized)
                    return (
                        <Redirect
                            to={{
                                pathname: `/${PATH.AUTH.LOGIN}`,
                                // ...(route.location.pathname && { search: `?from=${route.location.pathname}` }),
                            }}
                        />
                    );
                return <Component {...props} />;
            }}
        />
    );
};

PrivateRoute.propTypes = {
    component: PropTypes.elementType.isRequired,
    needOrganization: PropTypes.bool,
};

PrivateRoute.defaultProps = {
    needOrganization: false,
};

export default PrivateRoute;
