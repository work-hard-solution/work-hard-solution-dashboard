import { RegisterFormContainer } from 'containers';
import AuthLayout from 'Layout/auth';
import React from 'react';

const RegisterPage = () => {
    return (
        <AuthLayout>
            <RegisterFormContainer />
        </AuthLayout>
    );
};

export default RegisterPage;
