import { ForgotPasswordContainer } from 'containers';
import AuthLayout from 'Layout/auth';
import React from 'react';

const ForgotPasswordPage = () => {
    return (
        <AuthLayout>
            <ForgotPasswordContainer />
        </AuthLayout>
    );
};

export default ForgotPasswordPage;
