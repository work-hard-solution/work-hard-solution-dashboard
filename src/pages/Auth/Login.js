import { LoginFormContainer } from 'containers';
import AuthLayout from 'Layout/auth';
import React from 'react';

const LoginPage = () => {
    return (
        <AuthLayout>
            <LoginFormContainer />
        </AuthLayout>
    );
};

export default LoginPage;
