import { createMuiTheme } from '@material-ui/core/styles';
// Fonts
const AvenirNextBoldWOFF = '/fonts/AvenirNext-Bold.woff';
const AvenirNextDemiBoldWOFF = '/fonts/AvenirNext-DemiBold.woff';
const AvenirNextHeavyWOFF = '/fonts/AvenirNext-Heavy.woff';
const AvenirNextMediumWOFF = '/fonts/AvenirNext-Medium.woff';
const AvenirNextRegularWOFF = '/fonts/AvenirNext-Regular.woff';
const DomineBoldTTF = '/fonts/Domine-Bold.ttf';
const DomineRegularTTF = '/fonts/Domine-Regular.ttf';
/**
 * Material UI theme
 * https://material-ui.com/customization/palette/
 */
/**
 * Material UI theme
 * https://material-ui.com/customization/palette/
 */
const AvenirNextBold = {
    fontFamily: 'Avenir Next Bold',
    src: `url(${AvenirNextBoldWOFF}) format("woff")`,
};
const AvenirNextDemiBold = {
    fontFamily: 'Avenir Next Demi Bold',
    src: `url(${AvenirNextDemiBoldWOFF}) format("woff")`,
};
const AvenirNextHeavy = {
    fontFamily: 'Avenir Next Heavy',
    src: `url(${AvenirNextHeavyWOFF}) format("woff")`,
};
const AvenirNextMedium = {
    fontFamily: 'Avenir Next Medium',
    src: `url(${AvenirNextMediumWOFF}) format("woff")`,
};
const AvenirNextRegular = {
    fontFamily: 'Avenir Next Regular',
    src: `url(${AvenirNextRegularWOFF}) format("woff")`,
};
const DomineBold = {
    fontFamily: 'Domine Bold',
    src: `url(${DomineBoldTTF}) format("truetype")`,
};
const DomineRegular = {
    fontFamily: 'Domine Regular',
    src: `url(${DomineRegularTTF}) format("truetype")`,
};

// Theme
const theme = createMuiTheme({
    // Font
    fonts: {
        h1: 35,
        h2: 28,
        h3: 20,
        h4: 17,
        h5: 15,
        large: 16,
        small: 14,
        tiny: 12,
        superTiny: 12,
        bold: 'bold',
        none: 'none',
        center: 'center',
    },
    radius: { xs: 4, input: 5, sm: 8, mid: 12, md: 16, lg: 24, xl: 32 },
    sizes: {
        xss: '0.6rem',
        xs: '0.875rem',
        sm: '1rem',
        md: '1.5rem',
        lg: '2rem',
        xl: '3rem',

        title: '1.2rem',
        primary: '0.875rem',
        small: 'small',
        lineHeight: '1.2rem',
    },
    palette: {
        background: {
            default: 'rgba(250, 250, 250, 1)',
        },

        colors: {
            // White
            white: '#ffffff',
            white_gray: '#f8f8f8',
            white_gray_light: '#fdfdfd',
            white_gray_unable: '#dfdfe4',
            white_commnet_background: '#f2f2f2',
            white_line: '#ededf0',
            white_borer: '#e2e8f0',

            // Gray
            gray_placeholder: '#ACACAC',
            gray_border: '#D3D5D8',
            gray: {
                50: '#fdfdfd',
                75: '#808080',
                100: '#acacac',
                200: '#F2F5F7',
                300: '#D3D5D8',
                400: '#F4F4F4',
                A700: '#616161',
            },

            // Black
            black: '#000000',
            black_gray: '#757575',
            black_gray_light: '#bfbfbf',
            black_normal_text: '#4a4a4a',
            green: '#169f84',
            green_light: '#29d7b4',
            green_dark: '#118c74',

            // Orange
            orange: '#f15b00',
            orange_light: '#fc9700',
            orange_super_light: '#fed16e',

            // Red
            red: '#eb4141',
            red_danger: '#f44336',
            error: {
                light: '#e57373',
                main: '#E02020',
                dark: '#d32f2f',
            },
            // Pink
            pink_light: '#fff3f3',

            // Transparent
            transparent: 'transparent',

            // srgb
            blue_rgba: 'rgba(104, 132, 242, 0.7)',
            purple_rgba: 'rgba(194, 107, 212, 0.7)',
            green_rgba: 'rgba(40, 190, 55, 0.7)',
            moss_rgba: 'rgba(83, 131, 51, 0.5)',
            coral_rgba: 'rgba(252, 137, 89, 0.7)',
            red_rgba: 'rgba(210, 77, 87, 0.7)',
            border_color_rgba: 'rgb(37, 77, 142)',

            // hover
            hover: '#EAEEFA',
        },

        textColors: {
            white: '#ffffff',
            base: '#0C192D',
            primary: '#0F1050',
            active: '#496DF5',
            error: '#d24d57',
            url: '#254D8E',
        },
        primary: {
            main: '#757575',
            light: '#bfbfbf',
            greenDark: '#118c74',
            contrastText: '#000000',
        },
    },
    status: {
        danger: '#f44336',
    },
    fontsFamily: {
        medium: 'Avenir Next Medium',
        bold: 'Avenir Next Bold',
        regular: 'Avenir Next Regular',
        demiBold: 'Avenir Next Demi Bold',
        heavy: 'Avenir Next Heavy',
        domineBold: 'Domine Bold',
        domineRegular: 'Domine Regular',
    },
    typography: {
        fontFamily: 'Avenir Next Medium',
    },
    shadow: {
        base: 'rgba(87, 88, 215, 0.25)',
        baseHover: 'rgba(87, 88, 215, 0.25)',
        primary: 'rgba(0, 0, 0, 0.1)',
        blue: 'rgba(104, 132, 242, 0.6)',
        purple: 'rgba(194, 107, 212, 0.6)',
        green: 'rgba(40, 190, 55, 0.6)',
        moss: 'rgba(83, 131, 51, 0.6)',
        coral: 'rgba(252, 137, 89, 0.6)',
        red: 'rgba(210, 77, 87, 0.6)',
        gray_base: 'rgb(211, 213, 216)',
    },
    border_gray: '#dfdfe4',
    border_error: '#dfdfe4',
    overrides: {
        MuiCssBaseline: {
            '@global': {
                body: {
                    WebkitFontSmoothing: 'antialiased',
                    MozOsxFontSmoothing: 'grayscale',
                    backgroundColor: '#fff',
                },
                '@font-face': [
                    AvenirNextBold,
                    AvenirNextDemiBold,
                    AvenirNextHeavy,
                    AvenirNextMedium,
                    AvenirNextRegular,
                    DomineBold,
                    DomineRegular,
                ],
            },
        },
    },
});
export default theme;
