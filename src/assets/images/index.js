/* eslint-disable global-require */
// import loginBackground from './auth/auth-login.jpg';
import loginBackground from './auth/auth-login-background.jpg';
// import loginBackground from './auth/auth-login-background1.jpg';
// Icons
import githubIcon from './icons/github.svg';
import googleIcon from './icons/google.svg';
import linkedinIcon from './icons/linkedin.svg';
import logo from './icons/work-hard-logo.png';

export const images = {
    login_background: {
        url: loginBackground,
    },
    register_background: {
        url: require('./auth/auth.svg'),
    },
    forget_background: {
        url: require('./auth/forget.svg'),
    },

    // Icons
    google_icon: {
        src: googleIcon,
    },
    github_icon: {
        src: githubIcon,
    },
    linkedin_icon: {
        src: linkedinIcon,
    },
    logo: {
        src: logo,
    },
};

export const imgSizes = {
    icon: '20px',
    avatar: '32px',
    avatarLarge: '48px',
    button: '48px',
};
