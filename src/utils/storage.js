const authUtils = () => {
    /**
     * Get a session key from localStorage.
     * @param {string} sessionKey Key used for retrieval
     */
    const getSessionKey = (sessionKey = '') => localStorage.getItem(sessionKey);

    /**
     * Get a session based on multiple keys stored in a persistent storage.
     * @param {*} sessionKeys List of keys used for retrieval
     * @param {*} transform Transform function used on each session key.
     */
    const getSession = (sessionKeys = [], transform = (item) => (item ? JSON.parse(item) : item)) => {
        const session = {};

        sessionKeys.forEach((sessionKey) => {
            session[sessionKey] = transform(getSessionKey(sessionKey));
        });

        return session;
    };

    /**
     * Clear everything related from localStorage. Used for cleaning-up operations
     * during logout.
     */
    const clearSession = (sessionKeys = []) => {
        sessionKeys.forEach((key) => {
            localStorage.removeItem(key);
        });
    };

    /**
     * Check whether one or more keys exist inside localStorage.
     * @param {array} sessionKeys
     */
    const authenticate = (sessionKeys = []) => sessionKeys.every((sessionKey) => localStorage.getItem(sessionKey));

    return { getSession, authenticate, clearSession };
};

export default authUtils;
