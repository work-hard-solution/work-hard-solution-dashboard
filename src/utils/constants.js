module.exports = {
    DEBOUNCE_REQUEST_TIME: 500,
    // COMMON
    TYPES: {
        FUNCTION: 'function',
        PASSWORD: 'password',
        PHONE: 'phone',
        TEXT: 'text',
        ACCESS_TOKEN: 'access_token',
        SEARCH: 'search',
    },
    SIZE: {
        MEDIUM: 'medium',
        LARGE: 'large',
        SMALL: 'small',
    },
    METHODS: {
        GET: 'get',
        POST: 'post',
        PUT: 'put',
        PATCH: 'patch',
        DELETE: 'delete',
    },
    PATH: {
        AUTH: {
            LOGIN: '/login',
            REGISTER: '/signup',
            FORGOT_PASSWORD: '/forgot-password',
            RESET_PASSWORD: '/reset-password',
        },
        // ORGANIZATION: {
        //     // Organization
        //     REGISTER: '/register-organization',
        //     CHOOSE: '/choose-your-organization',

        // },
    },
    PAGES: {
        NAME: {
            AUTH: {
                LOGIN: 'login',
                REGISTER: 'register',
                FORGOT_PASSWORD: 'forgot password',
                RESET_PASSWORD: 'reset password',
            },
        },
    },
};
