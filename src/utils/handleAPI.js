import axios from 'axios';
import { isEqual } from 'lodash';

// Constants
import { METHODS } from './constants';

axios.defaults.baseURL = process.env.REACT_APP_BASE_URL_API;

const request = [METHODS.GET, METHODS.POST, METHODS.PUT, METHODS.DELETE].reduce((result, method) => {
    result[method] = async (url, data = {}, configs = {}, organizationId) => {
        const accessToken = window.localStorage.getItem('accessToken');
        if (accessToken) configs.headers.Authorization = `Bearer ${accessToken}`;
        if (isEqual(method, METHODS.GET)) configs.params = data;
        if (organizationId) configs.headers.organization_id = organizationId;
        try {
            const response = await axios({
                method,
                url,
                data,
                ...configs,
            });
            return response.data;
        } catch (e) {
            throw e?.response?.data?.error || e;
        }
    };
    return result;
}, {});

export default request;
