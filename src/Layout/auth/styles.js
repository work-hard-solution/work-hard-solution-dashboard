import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(() => ({
    container: {
        width: '100%',
        '& .MuiGrid-container': {
            padding: 0,
        },
    },
    layout: {
        width: '100%',
        maxWidth: '380px',
        margin: '70px auto',
        textAlign: 'center',
    },
    marginLayout: {
        margin: 'auto',
    },
    images: {
        width: '33.33%',
        height: '100vh',
        objectFit: 'cover',
        position: 'fixed',
    },
    logoContainer: {
        width: '50%',
        margin: 'auto auto 30px auto',
    },
    logo: {
        width: '100%',
    },
}));
