// Material
import Grid from '@material-ui/core/Grid';
// Image
import { images } from 'assets/images';
// React
import React from 'react';

// Styles
import { useStyles } from './styles';

const AuthLayout = ({ children }) => {
    const classes = useStyles();

    return (
        <Grid className={classes.container} spacing={0} container>
            <Grid xs={4} item>
                <img alt="background" className={classes.images} src={images.login_background.url} />
            </Grid>
            <Grid className={classes.layout} xs={8} item>
                <div className={classes.logoContainer}>
                    <img alt="logo" className={classes.logo} src={images.logo.src} />
                </div>
                {children}
            </Grid>
        </Grid>
    );
};

export default AuthLayout;
