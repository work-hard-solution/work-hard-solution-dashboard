// import './i18n';

// Material
import { CssBaseline } from '@material-ui/core';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from 'assets/theme';
// React
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';

import App from './App';
import configureStore from './redux/store';
import * as serviceWorker from './utils/serviceWorker';

const store = configureStore({});

ReactDOM.render(
    <Provider store={store}>
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <BrowserRouter>
                <PersistGate loading={null} persistor={store.__PERSISTOR}>
                    <App />
                </PersistGate>
            </BrowserRouter>
        </ThemeProvider>
    </Provider>,
    document.getElementById('root'),
);

serviceWorker.unregister();
