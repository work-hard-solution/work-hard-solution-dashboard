/* eslint-disable react/no-unescaped-entities */
/* eslint-disable import/no-unresolved */
// Library
import { yupResolver } from '@hookform/resolvers/yup';
// Material
import Grid from '@material-ui/core/Grid';
// Images
import { images } from 'assets/images';
// Components
import Button from 'components/base/Button';
import Input from 'components/base/Input';
import Link from 'components/base/Link';
import Typography from 'components/base/Typography';
// Library
import { noop } from 'lodash';
import PropTypes from 'prop-types';
// React
import React from 'react';
import { FormProvider, useForm } from 'react-hook-form';

// Styles
import { useStyles } from './styles';
// Validation
import { loginSchema } from './validation';

export const LoginForm = ({ onSubmit, submitting, error }) => {
    const classes = useStyles();

    const methods = useForm({
        resolver: yupResolver(loginSchema),
    });
    const { handleSubmit, errors } = methods;

    return (
        <FormProvider {...methods}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <Typography variant="header">Welcome back</Typography>
                <Grid className={classes.containerHorizontal} justify="center" container>
                    <Typography variant="subTitle">New to Work Hard? </Typography>
                    <Link className={classes.linkExtend} to="/signup">
                        Sign up
                    </Link>
                </Grid>

                {/* Error message */}
                {/* {error && (
                    <Typography className={classes.errorMessage} color="error" component="div">
                        {error}
                    </Typography>
                )} */}

                <div className={classes.containerEmail}>
                    <Input error={errors.id?.message} id="id" name="id" placeholder="Your email address" />
                </div>
                <div className={classes.containerPassword}>
                    <Input
                        error={errors.password?.message}
                        id="password"
                        name="password"
                        placeholder="Password"
                        type="password"
                    />
                </div>
                <Button className={classes.signIn} color="primary" loading={submitting && error} type="submit">
                    Sign in
                </Button>
                <Grid justify="flex-end" container>
                    <Link className={classes.forgotPassword} color="primary" to="/forgot-password">
                        Forgot your password?
                    </Link>
                </Grid>
                <Grid className={classes.captionExtended}>
                    <Typography color="light" variant="caption">
                        Or login with
                    </Typography>
                </Grid>

                {/* Login by oauth2 */}
                <Grid className={classes.groupAuth} justify="space-between" container>
                    <Grid item>
                        <Button
                            alt="linked in"
                            className={classes.oauth2}
                            src={images.linkedin_icon.src}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item>
                        <Button
                            alt="google"
                            className={classes.oauth2}
                            src={images.google_icon.src}
                            variant="outlined"
                        />
                    </Grid>
                    <Grid item>
                        <Button
                            alt="github"
                            className={classes.oauth2}
                            src={images.github_icon.src}
                            variant="outlined"
                        />
                    </Grid>
                </Grid>
                <Typography className={classes.term} color="light" component="div" variant="small">
                    By continue, you're agreeing to out
                    <Link className={classes.termAndPolicy} color="primary" to="/term">
                        Terms of Use
                    </Link>{' '}
                    and
                    <Link className={classes.termAndPolicy} color="primary" to="/policy">
                        Privacy policy
                    </Link>
                </Typography>
            </form>
        </FormProvider>
    );
};

LoginForm.propTypes = {
    onSubmit: PropTypes.func,
    submitting: PropTypes.bool,
    error: PropTypes.string,
};

LoginForm.defaultProps = {
    onSubmit: noop,
    error: '',
    submitting: false,
};
