import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    containerHorizontal: {
        display: 'flex',
        margin: '20px 0 40px 0',
        fontSize: theme.fonts.large,
    },
    linkExtend: {
        fontFamily: theme.fontsFamily.bold,
        color: '#254D8E',
        lineHeight: '22px',
        marginLeft: '5px',
    },
    forgotPassword: {
        fontFamily: theme.fontsFamily.regular,
        color: '#254D8E',
        fontSize: theme.fonts.large,
        marginTop: '20px',
    },
    errorMessage: {
        marginBottom: '15px',
        textAlign: 'center',
        color: theme.status.danger,
    },
    containerEmail: {
        width: '100%',
        marginBottom: '30px',
    },
    containerPassword: {
        width: '100%',
    },
    signIn: {
        width: '100%',
        fontFamily: theme.fontsFamily.demiBold,
        marginTop: '40px',
        padding: '10px 27px',
    },
    captionExtended: {
        textAlign: 'left',
        marginTop: '70px',
    },
    groupAuth: {
        marginTop: '11px',
    },
    oauth2: {
        padding: '18px 37px',
        backgroundColor: 'transparent',
    },
    term: {
        color: 'rgba(12,25,45,0.57)',
        marginTop: '40px',
        fontSize: '13px',
        textAlign: 'left',
    },
    termAndPolicy: {
        marginLeft: '5px',
        color: '#254D8E',
        textDecoration: 'none',
    },
}));
