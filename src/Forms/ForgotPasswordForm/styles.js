import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    description: {
        fontFamily: theme.fontsFamily.regular,
        colors: theme.palette.colors.gray,
        margin: '30px 0 45px 0',
    },
    sendPassword: {
        width: '100%',
        fontFamily: theme.fontsFamily.medium,
        marginTop: '40px',
        marginBottom: '40px',
        padding: '10px 27px',
    },
    backToLogin: {
        fontFamily: theme.fontsFamily.bold,
        color: '#254D8E',
        fontSize: theme.fonts.large,
    },
}));
