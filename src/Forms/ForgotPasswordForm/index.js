/* eslint-disable react/no-unescaped-entities */
// import * as _ from 'lodash';
import { yupResolver } from '@hookform/resolvers/yup';
// Components
import Button from 'components/base/Button';
import Input from 'components/base/Input';
import Link from 'components/base/Link';
import Typography from 'components/base/Typography';
// Library
import { noop } from 'lodash';
import PropTypes from 'prop-types';
// React
import React from 'react';
import { FormProvider, useForm } from 'react-hook-form';

// Styles
import { useStyles } from './styles';
// Validation
import { forgotPasswordSchema } from './validation';

export const ForgotPasswordForm = ({ submitting, error, onSubmit }) => {
    const classes = useStyles();

    const methods = useForm({
        resolver: yupResolver(forgotPasswordSchema),
    });

    const { errors, handleSubmit } = methods;

    return (
        <FormProvider {...methods}>
            <Typography variant="header">Forgot password ?</Typography>
            <Typography className={classes.description}>
                Don't worry, just enter the email address you registered with and we will send you a ink to reset your
                password
            </Typography>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <Input error={errors.id?.message} id="id" name="id" placeholder="Your email address" />
                </div>
                <Button className={classes.sendPassword} color="primary" loading={submitting && error} type="submit">
                    Send password reset link
                </Button>
                <Link className={classes.backToLogin} to="/login">
                    Back to login
                </Link>
            </form>
        </FormProvider>
    );
};

ForgotPasswordForm.propTypes = {
    onSubmit: PropTypes.func,
    submitting: PropTypes.bool,
    error: PropTypes.string,
};

ForgotPasswordForm.defaultProps = {
    submitting: false,
    onSubmit: noop,
    error: '',
};
