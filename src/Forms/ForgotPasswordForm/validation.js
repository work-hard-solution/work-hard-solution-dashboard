import * as yup from 'yup';

export const forgotPasswordSchema = yup.object().shape({
    id: yup.string().email('Must be an email').max(128).required('Please fill out this field'),
});
