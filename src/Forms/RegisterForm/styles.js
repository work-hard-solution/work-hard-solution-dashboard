import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    containerHorizontal: {
        display: 'flex',
        margin: '20px 0 40px 0',
        fontSize: theme.fonts.large,
    },
    input: {
        marginBottom: '20px',
    },
    linkExtend: {
        fontFamily: theme.fontsFamily.bold,
        color: '#254D8E',
        marginLeft: '5px',
    },
    signUp: {
        width: '100%',
        fontFamily: theme.fontsFamily.demiBold,
        marginTop: '40px',
        padding: '10px 27px',
    },
    term: {
        color: 'rgba(12,25,45,0.57)',
        marginTop: '40px',
        fontSize: '13px',
        textAlign: 'left',
    },
    termAndPolicy: {
        marginLeft: '5px',
        color: '#254D8E',
        textDecoration: 'none',
    },
}));
