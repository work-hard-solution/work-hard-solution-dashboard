/* eslint-disable react/no-unescaped-entities */
/* eslint-disable import/no-unresolved */
import { yupResolver } from '@hookform/resolvers/yup';
// Material
import Grid from '@material-ui/core/Grid';
// Components
import Button from 'components/base/Button';
import Input from 'components/base/Input';
import Link from 'components/base/Link';
import Typography from 'components/base/Typography';
// Library
import { noop } from 'lodash';
import PropTypes from 'prop-types';
// React
import React from 'react';
import { FormProvider, useForm } from 'react-hook-form';

// Styles
import { useStyles } from './styles';
// Validation
import { registerSchema } from './validation';

export const RegisterForm = ({ onSubmit, submitting, error }) => {
    const classes = useStyles();

    const methods = useForm({
        resolver: yupResolver(registerSchema),
    });

    const { handleSubmit, errors } = methods;

    return (
        <FormProvider {...methods}>
            <form onSubmit={handleSubmit(onSubmit)}>
                <Typography variant="header">Be more productive</Typography>
                <Grid className={classes.containerHorizontal} justify="center" container>
                    <Typography variant="subTitle">Already signed up? </Typography>
                    <Link className={classes.linkExtend} to="/login">
                        Log in
                    </Link>
                </Grid>

                {/* Error message */}
                {error && (
                    <Typography className={classes.errorMessage} color="error" component="div">
                        {error}
                    </Typography>
                )}

                <Grid className={classes.input} component="div" spacing={1} container>
                    <Grid xs={6} item>
                        <Input error={errors.first_name?.message} id="id" name="first_name" placeholder="First name" />
                    </Grid>
                    <Grid xs={6} item>
                        <Input error={errors.last_name?.message} id="id" name="last_name" placeholder="Last name" />
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid className={classes.input} xs={12} item>
                        <Input error={errors.email?.message} id="email" name="email" placeholder="Your email address" />
                    </Grid>
                    <Grid xs={12} item>
                        <Input
                            error={errors.password?.message}
                            id="password"
                            name="password"
                            placeholder="Create a password"
                            type="password"
                        />
                    </Grid>
                </Grid>
                <Button className={classes.signUp} color="primary" loading={submitting && error} type="submit">
                    Sign up
                </Button>

                <Typography className={classes.term} color="light" component="div" variant="small">
                    By continue, you're agreeing to out
                    <Link className={classes.termAndPolicy} color="primary" to="/term">
                        Terms of Use
                    </Link>{' '}
                    and
                    <Link className={classes.termAndPolicy} color="primary" to="/policy">
                        Privacy policy
                    </Link>
                </Typography>
            </form>
        </FormProvider>
    );
};

RegisterForm.propTypes = {
    onSubmit: PropTypes.func,
    submitting: PropTypes.bool,
    error: PropTypes.string,
};

RegisterForm.defaultProps = {
    onSubmit: noop,
    error: '',
    submitting: false,
};
