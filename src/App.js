import React, { Suspense } from 'react';
import { Helmet } from 'react-helmet';
import { Route, Switch } from 'react-router-dom';

import { APP_NAME } from './configs/constants';
import PrivateRoute from './routers/privateRoute';
import { publicPages } from './routers/routes';

function App() {
    return (
        <>
            <Helmet defaultTitle={APP_NAME} titleTemplate={`${APP_NAME} | %s`} />
            <Suspense fallback={<div>Loading...</div>}>
                <Switch>
                    {publicPages.map((route) =>
                        route.auth ? (
                            <PrivateRoute key={route.path} {...route} />
                        ) : (
                            <Route key={route.path} {...route} />
                        ),
                    )}
                </Switch>
            </Suspense>
        </>
    );
}

export default App;
