// Material
import Box from '@material-ui/core/Box';
// Library
import PropTypes from 'prop-types';
// React
import React from 'react';

// Styles
import { useStyles } from './styles';

const PopoverPassword = ({ hasFocus }) => {
    const classes = useStyles({ hasFocus });

    return (
        <div className={classes.wrapper}>
            <Box classes={{ root: classes.focusDescriptionsWrapper }}>
                <span className={classes.triangle} />
                <div className={classes.descriptionText}>
                    Password must contain a <strong>letter </strong> and a <strong>number</strong>, and minimum of
                    <strong> 9 characters</strong>
                </div>
            </Box>
        </div>
    );
};

PopoverPassword.propTypes = {
    hasFocus: PropTypes.bool,
};

PopoverPassword.defaultProps = {
    hasFocus: false,
};

export default PopoverPassword;
