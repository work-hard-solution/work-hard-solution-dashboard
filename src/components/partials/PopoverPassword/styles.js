import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(
    (theme) => ({
        wrapper: {
            paddingTop: ({ hasFocus }) => (hasFocus ? 10 : 0),
            transition: 'all 0.15s',
        },
        focusDescriptionsWrapper: {
            transition: 'all 0.15s',
            height: ({ hasFocus }) => (hasFocus ? 63 : 0),
            width: '100%',
            padding: ({ hasFocus }) => (hasFocus ? '0.75rem' : '0 0.75rem'),
            backgroundColor: 'rgb(242, 244, 247)',
            position: 'relative',
            border: ({ hasFocus }) =>
                hasFocus
                    ? `1px  solid  ${theme.palette.colors.gray_border}`
                    : `0 solid ${theme.palette.colors.gray_border}`,
            borderRadius: theme.radius.xs,
        },
        triangle: {
            transform: 'translateX(-50%) rotate(45deg)',
            transition: 'all 0.15s',
            position: 'absolute',
            opacity: ({ hasFocus }) => (hasFocus ? 1 : 0),
            left: 25,
            top: -7,
            width: 14,
            height: 14,
            backgroundColor: 'rgb(242, 244, 247)',
            borderStyle: 'solid',
            borderWidth: 1,
            borderColor: `${theme.palette.colors.gray_border} transparent transparent
        ${theme.palette.colors.gray_border}`,
        },
        descriptionText: {
            transition: 'all 0.15s',
            textAlign: 'left',
            color: theme.palette.text.main,
            opacity: ({ hasFocus }) => (hasFocus ? 1 : 0),
            margin: 0,
            fontFamily: theme.fontsFamily.medium,
            fontSize: 14,
            lineHeight: '19px',
            letterSpacing: 0,
        },
    }),
    { name: 'FocusDescription' },
);
