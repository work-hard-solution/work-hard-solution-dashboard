import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        boxShadow: theme.fonts.none,
        borderRadius: theme.radius.input,
        textTransform: theme.fonts.none,
        alignItems: theme.fonts.center,
        justifyContent: theme.fonts.center,
        cursor: 'pointer',
        color: theme.palette.colors.white,
        backgroundColor: theme.palette.colors.black,
        height: 'auto',
        fontFamily: theme.fontsFamily.demiBold,
        fontSize: '16px',
        // lineHeight: '22px',
        '&:hover': {
            backgroundColor: theme.palette.colors.black_gray_light,
        },
        // '&:hover': {},
    },
    '&:active': {
        boxShadow: theme.fonts.none,
    },
    '&:focus': {
        outline: 'none !important',
    },
    label: {
        fontSize: theme.fonts.large,
        color: theme.palette.textColors.white,
        fontWeight: 600,
        '& img': {
            width: '25px',
        },
    },
}));
