// Material
import MuiTypography from '@material-ui/core/Typography';
// Library
import clsx from 'clsx';
import PropTypes from 'prop-types';
// React
import React from 'react';

// Styles
import { useStyles } from './styles';

const Typography = (typoProps) => {
    const { className, variant, children, id, ...props } = typoProps;

    const classes = useStyles();

    return (
        <MuiTypography className={clsx(className, classes[variant])} id={id} {...props}>
            {children}
        </MuiTypography>
    );
};

Typography.propTypes = {
    className: PropTypes.string,
    id: PropTypes.number,
    variant: PropTypes.oneOf(['default']),
};

Typography.defaultProps = {
    className: '',
    variant: 'default',
    id: 0,
};

export default Typography;
