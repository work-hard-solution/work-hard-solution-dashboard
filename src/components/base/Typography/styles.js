import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    header: {
        fontSize: theme.fonts.h3,
        fontFamily: theme.fontsFamily.demiBold,
    },
    default: {
        fontFamily: theme.fontsFamily.medium,
    },
    subTitle: {
        fontSize: theme.fonts.large,
    },
    caption: {
        color: 'rgb(107, 114, 124)',
    },
    error: {
        color: theme.status.danger,
    },
}));
