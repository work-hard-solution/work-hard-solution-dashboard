import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    container: {
        width: '100%',
        '& .MuiInput-underline:before': {
            display: 'none',
        },
        '& .MuiInput-underline:after': {
            display: 'none',
        },
        '& .MuiInputBase-input': {
            // Reset to default style
            lineHeight: 0,
            padding: 0,
            height: '1.375em',
        },
        position: 'relative',
    },
    root: {
        padding: '0 !important',
    },
    input: {
        width: '100%',
        borderRadius: theme.radius.input,
        border: ({ error }) =>
            error ? `1px solid ${theme.palette.error.main}` : `1px solid ${theme.palette.colors.gray_border}`,
        padding: theme.fonts.tiny,
        fontFamily: theme.fontsFamily.medium,
        fontSize: theme.sizes.sm,
        color: theme.palette.textColors.base,
        backgroundColor: theme.palette.colors.gray['50'],
        '&::placeholder': {
            fontSize: theme.fonts.small,
            color: theme.palette.colors.gray_placeholder,
            fontWeight: 500,
        },
    },
    icon: {
        fontSize: theme.sizes.md,
    },
    showPassword: {
        position: 'absolute',
        right: 0,
        top: 0,
        bottom: 0,
        height: '100%',
        display: 'flex',
        alignItems: 'center',
    },
    error: {
        display: 'flex',
        alignItems: 'center',
        fontSize: 14,
        color: theme.palette.colors.error.main,
        marginTop: 12,
        textAlign: 'left',
    },
}));
