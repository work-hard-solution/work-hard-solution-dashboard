// Material
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import MuiInput from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import clsx from 'clsx';
// Components
import Typography from 'components/base/Typography';
import PopoverPassword from 'components/partials/PopoverPassword';
// Library
import { isEqual, noop } from 'lodash';
import PropTypes from 'prop-types';
// React
import React, { useState } from 'react';
import { useFormContext } from 'react-hook-form';
// Constants
import { TYPES } from 'utils/constants';

// Styles
import { useStyles } from './styles';

const Input = ({ id, ref, label, error, className, type, placeholder, onFocus, onBlur }) => {
    const classes = useStyles({ error });
    // Ref to form
    const { register } = useFormContext();

    const [hasFocus, setFocus] = useState(false);
    const [showPassword, setShowPassword] = useState(false);

    const handleBlur = (e) => {
        setFocus(false);
        if (isEqual(typeof onBlur, TYPES.FUNCTION)) {
            onBlur(e);
        }
    };

    const handleFocus = (e) => {
        setFocus(true);
        if (isEqual(typeof onFocus, TYPES.FUNCTION)) {
            onFocus(e);
        }
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const togglePasswordVisibility = () => {
        setShowPassword((prevState) => !prevState);
    };

    return (
        <Grid alignItems="center" className={classes.container} error={error} status="error" type={type} container>
            <MuiInput
                className={clsx(className, classes.input)}
                endAdornment={
                    isEqual(type, TYPES.PASSWORD) && (
                        <InputAdornment position="end">
                            <IconButton onClick={togglePasswordVisibility} onMouseDown={handleMouseDownPassword}>
                                {showPassword ? <Visibility /> : <VisibilityOff />}
                            </IconButton>
                        </InputAdornment>
                    )
                }
                id={id}
                inputRef={(el) => {
                    register(el);
                    if (ref && el) {
                        ref.current = el;
                    }
                }}
                label={label}
                name={id}
                placeholder={placeholder}
                type={showPassword ? TYPES.TEXT : type}
                onBlur={handleBlur}
                onFocus={handleFocus}
            />
            {isEqual(type, TYPES.PASSWORD) && <PopoverPassword hasFocus={error ? true : false || hasFocus} />}
            {error && (
                <Typography className={classes.error} color="error" component="p">
                    *{error}
                </Typography>
            )}
        </Grid>
    );
};

Input.propTypes = {
    className: PropTypes.string,
    id: PropTypes.number,
    label: PropTypes.string,
    type: PropTypes.string,
    error: PropTypes.number,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    ref: PropTypes.oneOfType([
        // Either a function
        PropTypes.func,
        // Or the instance of a DOM native element (see the note about SSR)
        PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
    ]),
    placeholder: PropTypes.string,
};

Input.defaultProps = {
    className: '',
    error: '',
    label: '',
    type: '',
    id: 0,
    onFocus: noop,
    onBlur: noop,
    placeholder: '',
    ref: undefined,
};

export default Input;
