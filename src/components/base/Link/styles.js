import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    container: {
        color: theme.palette.textColors.url,
        cursor: 'pointer',
    },
}));
