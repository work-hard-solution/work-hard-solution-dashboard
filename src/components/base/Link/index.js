// Material
import MuiLink from '@material-ui/core/Link';
// Library
import PropTypes from 'prop-types';
// React
import React from 'react';
import { Link as LinkRouter } from 'react-router-dom';

// Styles
import { useStyles } from './styles';

const Link = ({ blank, children, to, ...props }) => {
    const classes = useStyles();

    return blank ? (
        <MuiLink className={classes.container} {...props}>
            {children}
        </MuiLink>
    ) : (
        <LinkRouter className={classes.container} to={to} {...props}>
            {children}
        </LinkRouter>
    );
};

Link.propTypes = {
    blank: PropTypes.string,
    to: PropTypes.string,
};

Link.defaultProps = {
    blank: '',
    to: '',
};

export default Link;
